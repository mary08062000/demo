package org.example.controllers

import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Component
import server.apis.GreetingApi

@Component
class GreetingController : GreetingApi{
    override fun rootGet(): ResponseEntity<String> {
        val greeting = "Добро пожаловать! \n" +
                "Чтобы добавить имя, выполните POST запрос по пути /name относительно текущего с Json объектом с полем name, например {\"name\": \"Mary\"}. \n" +
                "Для получения имени выполните GET запрос по пути /name относительно текущего. \n" +
                "Ссылка на проект : https://gitlab.com/mary08062000/demo"
        return ResponseEntity.ok(greeting)
    }
}
