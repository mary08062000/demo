package org.example.controllers

import org.springframework.http.ResponseEntity
import org.example.repositories.NameRepository
import org.springframework.stereotype.Component
import server.apis.NameApi
import server.models.Name

@Component
class NameController(
    val nameRepository: NameRepository,
) : NameApi {

    override fun nameGet(): ResponseEntity<Name> {
        val name = nameRepository.name
        return if (name == null)
            ResponseEntity.notFound().build()
        else {
            ResponseEntity.ok(Name(name))
        }
    }

    override fun namePost(name: Name): ResponseEntity<Unit> {
        nameRepository.name = name.name
        return ResponseEntity.ok().build()
    }
}
